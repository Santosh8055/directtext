import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  phonenumber: number;

  startMessage() {
    window.location.href = `https://wa.me/${this.phonenumber}#`;
  }
}
